//
//  CurrenciesExchangeListCoordinator..swift
//  Revolut Test
//
//  Created by Marek Serafin on 05/02/2019.
//  Copyright © 2019 Marek Serafin. All rights reserved.
//

import UIKit
import Moya

// MARK: CurrenciesExchangeListCoordinator
final class CurrenciesExchangeListCoordinator {
    
    let navigationController: UINavigationController
    let provider = MoyaProvider<RevolutService>()
    
    init(_ navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let currenciesExchangeListViewController = CurrenciesExchangeListViewController.fromStoryboard()
        currenciesExchangeListViewController.title = NSLocalizedString("Revolut Test", comment: "")
        currenciesExchangeListViewController.viewModel = CurrenciesExchangeListViewModel(currency: .EUR, provider: provider)
        
        navigationController.pushViewController(currenciesExchangeListViewController, animated: false)
    }
}

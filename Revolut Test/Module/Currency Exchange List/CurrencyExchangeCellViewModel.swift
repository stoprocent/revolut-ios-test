//
//  CurrencyExchangeCellViewModel.swift
//  Revolut Test
//
//  Created by Marek Serafin on 06/02/2019.
//  Copyright © 2019 Marek Serafin. All rights reserved.
//

import Foundation
import Alamofire

// MARK: CurrencyExchangeCellViewModelDelegate
protocol CurrencyExchangeCellViewModelDelegate {
    func didUpdateRateValue(_ viewModel: CurrencyExchangeCellViewModel, value: String?)
}

// MARK: CurrencyExchangeCellViewModel
final class CurrencyExchangeCellViewModel {
    
    var delegate: CurrencyExchangeCellViewModelDelegate?
    
    var currency: Currency
    
    var amount: Double {
        didSet { value = formatValue(rate, amount) }
    }
    var inputAmount: String? = nil {
        didSet { inputAmountCallback?( formatInputAmount(inputAmount) ) }
    }
    var rate: Double {
        didSet { value = formatValue(rate, amount) }
    }
    var value: String? {
        didSet { delegate?.didUpdateRateValue(self, value: value) }
    }
    
    private var lock = NSLock()
    private var inputAmountCallback: ((Double) -> Void)?
    
    init(currency: Currency, rate: Double, amount: Double, inputAmountCallback: ((Double) -> Void)? = nil) {
        self.currency = currency
        self.rate = rate
        self.amount = amount
        self.value = formatValue(rate, amount)
        self.inputAmountCallback = inputAmountCallback
    }
    
    private func formatValue(_ rate: Double, _ amount: Double) -> String? {
        lock.lock(); defer { lock.unlock() }
        let total = rate * amount
        return total < 0.01 ? "" : CurrencyExchangeCellViewModel.formatter.string(from: NSNumber(value: total))
    }
    
    private func formatInputAmount(_ input: String?) -> Double {
        return CurrencyExchangeCellViewModel.formatter.number(from: input ?? "")?.doubleValue ?? 0.0
    }
}

// MARK: CurrencyExchangeCellViewModel Helper Extension
extension CurrencyExchangeCellViewModel {
    
    static let formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        formatter.minimumIntegerDigits = 1
        return formatter
    }()
}

//
//  CurrenciesExchangeListViewModel.swift
//  Revolut Test
//
//  Created by Marek Serafin on 04/02/2019.
//  Copyright © 2019 Marek Serafin. All rights reserved.
//

import Foundation
import Moya

// MARK: CurrenciesExchangeListViewModelDelegate
protocol CurrenciesExchangeListViewModelDelegate {
    func rateDidChange(_ viewModel: CurrenciesExchangeListViewModel, currency: Currency, rate: Double)
    func amountDidChange(_ viewModel: CurrenciesExchangeListViewModel, amount: Double)
    func currenciesDidChange(_ viewModel: CurrenciesExchangeListViewModel)
    func currenciesDidReorder(_ viewModel: CurrenciesExchangeListViewModel, index: Int, to: Int)
    func currenciesDidFail(_ viewModel: CurrenciesExchangeListViewModel, with error: Error)
}

// MARK: CurrenciesExchangeListViewModel
final class CurrenciesExchangeListViewModel {
    
    var isReady: Bool { return currencies.count > 0 }
    
    var delegate: CurrenciesExchangeListViewModelDelegate?
    
    var rates = [Currency: Double]()
    var currencies = [Currency]()
    
    var baseAmount: Double = 100.0 {
        didSet { delegate?.amountDidChange(self, amount: baseAmount) }
    }
    var baseCurrency: Currency = .unknown {
        didSet {
            guard oldValue != baseCurrency else { return }
            reorderCurrencies { [delegate = delegate] in delegate?.currenciesDidReorder(self, index: $0, to: 0) }
            scheduleRatesUpdate()
        }
    }
    
    var ratesUpdateInterval: Int
    
    private var provider: MoyaProvider<RevolutService>!
    private var currenciesHash = Int(0)
    private var scheduledRatesUpdateItem: DispatchWorkItem?
    
    init(currency: Currency, provider: MoyaProvider<RevolutService>, interval: Int = 1) {
        self.baseCurrency = currency
        self.provider = provider
        self.ratesUpdateInterval = interval
    }
    
    func scheduleRatesUpdate() {
        scheduledRatesUpdateItem?.cancel()
        
        self.provider.request(.exchangeRates(currency: self.baseCurrency)) { [weak self] result in
            guard let strongSelf = self else { return }

            switch result {
            case .success(let response):
                do {
                    let _ = try response.filterSuccessfulStatusCodes()
                    if let result = try? response.map(Rates.self) {
                        
                        strongSelf.processRatesUpdate(result)
                        
                        let workItem = DispatchWorkItem { strongSelf.scheduleRatesUpdate() }
                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(strongSelf.ratesUpdateInterval), execute: workItem)
                        
                        strongSelf.scheduledRatesUpdateItem = workItem
                    }
                    else {
                        strongSelf.delegate?.currenciesDidFail(strongSelf, with: RevolutServiceError.invalidResponse)
                    }
                }
                catch let error {
                    strongSelf.delegate?.currenciesDidFail(strongSelf, with: error)
                }
            case .failure(let error):
                strongSelf.delegate?.currenciesDidFail(strongSelf, with: error)
            }
            
        }
    }
    
    private func reorderCurrencies(_ callback: ((Int) -> Void)? = nil) {
        if let index = currencies.firstIndex(of: baseCurrency) {
            currencies.insert(currencies.remove(at: index), at: 0)
            callback?(index)
        }
    }
    
    private func processRatesUpdate(_ updated: Rates) {
        let updatedCurrencies = Set(updated.rates.keys)
        
        if currenciesHash == updatedCurrencies.hashValue {
            for (currency, rate) in updated.rates {
                rates[currency] = rate
                delegate?.rateDidChange(self, currency: currency, rate: rate)
            }
        } else {
            rates = updated.rates
            currencies = updatedCurrencies.sorted { $0.rawValue < $1.rawValue }
            currenciesHash = updatedCurrencies.hashValue
            
            reorderCurrencies()
            delegate?.currenciesDidChange(self)
        }
    }
}

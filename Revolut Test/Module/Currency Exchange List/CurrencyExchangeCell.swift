//
//  CurrencyExchangeCell.swift
//  Revolut Test
//
//  Created by Marek Serafin on 06/02/2019.
//  Copyright © 2019 Marek Serafin. All rights reserved.
//

import UIKit

// MARK: CurrencyExchangeCellDelegate
protocol CurrencyExchangeCellDelegate {
    func cellBecameFirstResponder(_ cell: CurrencyExchangeCell)
}

// MARK: CurrencyExchangeCell
final class CurrencyExchangeCell: UITableViewCell {
    
    static let identifier = "CurrencyExchangeCell"
    
    var delegate: CurrencyExchangeCellDelegate?
    
    @IBOutlet weak var flagImageView: UIImageView! {
        didSet {
            flagImageView.layer.cornerRadius = 23.0
            flagImageView.clipsToBounds = true
            flagImageView.backgroundColor = .revolutGray
            flagImageView.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var localizedNameLabel: UILabel!
    @IBOutlet weak var valueTextField: UITextField!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var borderViewHeight: NSLayoutConstraint!
    
    var viewModel: CurrencyExchangeCellViewModel?

    func setup(viewModel: CurrencyExchangeCellViewModel) {
        
        viewModel.delegate = self
        
        flagImageView.image = UIImage(named: viewModel.currency.imageName)
        nameLabel.text = viewModel.currency.rawValue
        localizedNameLabel.text = viewModel.currency.localized
        valueTextField.text = viewModel.value
        valueTextField.placeholder = "0"
        
        hideSkeleton()
    
        self.viewModel = viewModel
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        showAnimatedSkeleton()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        viewModel?.delegate = nil
        flagImageView?.image = nil
        
        showAnimatedSkeleton()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {        
        super.setSelected(selected, animated: animated)
        
        if selected { valueTextField.becomeFirstResponder() }
        
        UIView.animate(withDuration: 0.5) {
            self.borderView.backgroundColor = selected ? .revolutBlue : .revolutGray
            self.borderViewHeight.constant  = selected ? 2 : 1
            self.layoutIfNeeded()
        }
    }

}

// MARK: CurrencyExchangeCellViewModelDelegate
extension CurrencyExchangeCell: CurrencyExchangeCellViewModelDelegate {
    
    func didUpdateRateValue(_ viewModel: CurrencyExchangeCellViewModel, value: String?) {
        guard !valueTextField.isFirstResponder else { return }
        DispatchQueue.main.async { self.valueTextField.text = value }
    }
    
    func didUpdateFlagImage(with data: Data?) {
        DispatchQueue.global().async { [imageView = flagImageView] in
            let image = data != nil ? UIImage(data: data!) : nil
            DispatchQueue.main.async { imageView?.image = image }
        }
    }
}

// MARK: UITextFieldDelegate
extension CurrencyExchangeCell: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        viewModel?.inputAmount = textField.text
        delegate?.cellBecameFirstResponder(self)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = NSString(string: textField.text ?? "").replacingCharacters(in: range, with: string)
        if let regex = try? NSRegularExpression(pattern: "^[0-9]{0,10}((\\.|,)[0-9]{0,2})?$", options: .caseInsensitive) {
            return regex.numberOfMatches(in: text, options: .reportProgress, range: NSRange(location: 0, length: text.count)) > 0
        }
        return false
    }
    
    @IBAction func textFieldDidChange(_ textField: UITextField) {
        valueTextField.invalidateIntrinsicContentSize()
        
        guard textField.isFirstResponder else { return }
        viewModel?.inputAmount = textField.text
    }
}

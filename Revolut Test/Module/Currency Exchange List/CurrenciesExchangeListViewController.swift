//
//  CurrenciesExchangeListViewController.swift
//  Revolut Test
//
//  Created by Marek Serafin on 04/02/2019.
//  Copyright © 2019 Marek Serafin. All rights reserved.
//

import UIKit
import SkeletonView

// MARK: Custom Colors
extension UIColor {
    static let revolutBlue = UIColor(red:0.18, green:0.41, blue:0.70, alpha:1.00)
    static let revolutGray = UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.00)
}

// MARK: CurrenciesExchangeListViewController
final class CurrenciesExchangeListViewController: UITableViewController, Storyboarded {
    
    static var storyboardIdentifier = "CurrenciesExchangeList"
    
    var viewModel: CurrenciesExchangeListViewModel! {
        didSet {
            viewModel.delegate = self
            viewModel.scheduleRatesUpdate()
        }
    }
    
    var cellViewModels: [Currency: CurrencyExchangeCellViewModel] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

// MARK: CurrenciesExchangeListViewModelDelegate
extension CurrenciesExchangeListViewController: CurrenciesExchangeListViewModelDelegate {
    
    func currenciesDidChange(_ viewModel: CurrenciesExchangeListViewModel) {
        DispatchQueue.main.async {
            self.tableView.beginUpdates()
            self.tableView.reloadSections(IndexSet([0]), with: .automatic)
            self.tableView.endUpdates()
        }
    }
    
    func currenciesDidReorder(_ viewModel: CurrenciesExchangeListViewModel, index: Int, to: Int) {
        DispatchQueue.main.async {
            self.tableView.beginUpdates()
            self.tableView.moveRow(at: IndexPath(row: index, section: 0), to: IndexPath(row: to, section: 0))
            self.tableView.endUpdates()
            self.tableView.scrollToRow(at: IndexPath(row: to, section: 0), at: .top, animated: true)
        }
    }
    
    func currenciesDidFail(_ viewModel: CurrenciesExchangeListViewModel, with error: Error) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) { [weak self] in
            self?.viewModel.scheduleRatesUpdate()
        }
    }
    
    func rateDidChange(_ viewModel: CurrenciesExchangeListViewModel, currency: Currency, rate: Double) {
        guard let cellViewModel = cellViewModels[currency] else { return }
        cellViewModel.rate = rate
    }
    
    func amountDidChange(_ viewModel: CurrenciesExchangeListViewModel, amount: Double) {
        cellViewModels.forEach { $0.value.amount = amount }
    }
}

// MARK: CurrencyExchangeCellDelegate
extension CurrenciesExchangeListViewController: CurrencyExchangeCellDelegate {
    
    func cellBecameFirstResponder(_ cell: CurrencyExchangeCell) {
        guard let indexPath = tableView.indexPath(for: cell), !cell.isSelected else { return }
        
        tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        tableView(tableView, didSelectRowAt: indexPath)
    }
}

// MARK:
extension CurrenciesExchangeListViewController {
    
    func viewModelForRow(at indexPath: IndexPath) -> CurrencyExchangeCellViewModel {
        let currency = viewModel.currencies[indexPath.row]
        
        if let cellViewModel = cellViewModels[currency] {
            return cellViewModel
        }
        
        let cellViewModel = CurrencyExchangeCellViewModel(currency: currency, rate: viewModel.rates[currency]!, amount: viewModel.baseAmount) { [weak self] in
            self?.viewModel.baseAmount = $0
        }
        
        cellViewModels[currency] = cellViewModel
        
        return cellViewModel
    }
    
    /* This is just to show how to control models amount. With current setup memory footprint of all models in heap is about 5kb.
    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard indexPath.row > 0, viewModel.isReady else { return }
        cellViewModels.removeValue(forKey: viewModel.currencies[indexPath.row])
    }
    */
}

// MARK: UITableViewDelegate & Data Source
extension CurrenciesExchangeListViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.isReady ? viewModel.currencies.count: Int(floor(tableView.frame.height / tableView.estimatedRowHeight))
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CurrencyExchangeCell.identifier) as? CurrencyExchangeCell else { return UITableViewCell() }
        
        if viewModel.isReady {
            cell.setup(viewModel: viewModelForRow(at: indexPath))
            cell.delegate = self
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard viewModel.isReady else { return }
        viewModel.baseCurrency = viewModel.currencies[indexPath.row]
    }
}

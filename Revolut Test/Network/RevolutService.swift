//
//  RevolutService.swift
//  Revolut Test
//
//  Created by Marek Serafin on 04/02/2019.
//  Copyright © 2019 Marek Serafin. All rights reserved.
//

import Moya

enum RevolutServiceError: Error {
    case invalidResponse
}

enum RevolutService {
    case exchangeRates(currency: Currency)
}

extension RevolutService: TargetType {
    
    var baseURL: URL { return URL(string: "https://revolut.duckdns.org/")! }
    
    var path: String {
        switch self {
        case .exchangeRates:
            return "latest"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .exchangeRates:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case .exchangeRates(let currency):
            return .requestParameters(parameters: ["base": currency.rawValue], encoding: URLEncoding.queryString)
        }
    }

    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
    
    var sampleData: Data {
        switch self {
        case .exchangeRates(let currency):
            return "{\"base\":\"\(currency.rawValue)\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":2.0,\"BGN\":3.0,\"BRL\":4.0,\"CAD\":5.0}}".data(using: .utf8)!
        }
    }
    
}

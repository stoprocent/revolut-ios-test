//
//  Currency.swift
//  Revolut Test
//
//  Created by Marek Serafin on 05/02/2019.
//  Copyright © 2019 Marek Serafin. All rights reserved.
//

import Foundation

enum Currency: String, CaseIterable, Decodable {
    case unknown, AUD, BGN, BRL, CAD, CHF, CNY, CZK, DKK, EUR, GBP, HKD, HRK, HUF, IDR, ILS, INR, ISK, JPY, KRW, MXN, MYR, NOK, NZD, PHP, PLN, RON, RUB, SEK, SGD, THB, TRY, USD, ZAR
    
    var localized: String? {
        return NSLocalizedString(self.rawValue, comment: "")
    }
    
    var imageName: String {
        return rawValue.lowercased()
    }
}



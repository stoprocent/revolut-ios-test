//
//  Currency.swift
//  Revolut Test
//
//  Created by Marek Serafin on 05/02/2019.
//  Copyright © 2019 Marek Serafin. All rights reserved.
//

import Foundation

struct Rates: Decodable {
    
    var baseCurrency: Currency
    var rates: [Currency: Double]
    
    enum CodingKeys: String, CodingKey {
        case baseCurrency = "base", rates
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        baseCurrency = try values.decode(Currency.self, forKey: .baseCurrency)
        rates = Dictionary(uniqueKeysWithValues: try values.decode(Dictionary<String, Double>.self, forKey: .rates).compactMap { (Currency(rawValue: $0) ?? .unknown, $1)})
        rates[baseCurrency] = 1.0
    }
}

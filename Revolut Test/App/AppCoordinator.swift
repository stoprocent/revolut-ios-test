//
//  Coordinator.swift
//  Revolut Test
//
//  Created by Marek Serafin on 04/02/2019.
//  Copyright © 2019 Marek Serafin. All rights reserved.
//

import UIKit
import Moya

final class AppCoordinator {
    
    let window: UIWindow
    let navigationController: UINavigationController
    let currenciesExchangeListCooridinator: CurrenciesExchangeListCoordinator
    
    init(_ window: UIWindow) {
        self.window = window
        
        navigationController = UINavigationController()
        currenciesExchangeListCooridinator = CurrenciesExchangeListCoordinator(navigationController)
    }
    
    func start() {
        currenciesExchangeListCooridinator.start()
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
}

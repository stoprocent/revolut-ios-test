//
//  AppDelegate.swift
//  Revolut Test
//
//  Created by Marek Serafin on 04/02/2019.
//  Copyright © 2019 Marek Serafin. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow? = UIWindow()
    var appCoordinator: AppCoordinator? = nil

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Force unwrap `window` on purpose. If it fails something is seriously wrong with your application.
        appCoordinator = AppCoordinator(window!)
        appCoordinator?.start()
        
        return true
    }

    func application(_ application: UIApplication, shouldSaveApplicationState coder: NSCoder) -> Bool {
        return true
    }
    
    func application(_ application: UIApplication, shouldRestoreApplicationState coder: NSCoder) -> Bool {
        return true
    }
}


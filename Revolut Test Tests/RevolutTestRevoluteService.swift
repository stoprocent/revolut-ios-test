//
//  RevolutTestRevoluteService.swift
//  Revolut Test Tests
//
//  Created by Marek Serafin on 08/02/2019.
//  Copyright © 2019 Marek Serafin. All rights reserved.
//

import XCTest
import Moya
@testable import Revolut_Test

class RevolutTestRevoluteService: XCTestCase {
    
    let stubbingProvider = MoyaProvider<RevolutService>(stubClosure: MoyaProvider.immediatelyStub)
    
    func testSuccessResult() {
        
        stubbingProvider.request(.exchangeRates(currency: .EUR)) { result in
            switch result {
            case .failure(_):
                XCTAssert(false, "Expected successful result.")
            case .success(_):
                XCTAssert(true)
            }
        }
    }
    
    func testSuccessStatusCode() {
        
        stubbingProvider.request(.exchangeRates(currency: .EUR)) { result in
            switch result {
            case .failure(_):
                XCTAssert(false, "Expected successful result.")
            case .success(let result):
                XCTAssertTrue((200...299).contains(result.statusCode), "Expected status code to be 200...299.")
            }
        }
    }
    
    func testSuccessData() {
        
        stubbingProvider.request(.exchangeRates(currency: .EUR)) { result in
            switch result {
            case .failure(_):
                XCTAssert(false, "Expected successful result.")
            case .success(let result):
                XCTAssertNotNil(result.data, "Expected data to not be nil.")
                defer {
                    XCTAssertNoThrow(try result.map(Rates.self), "Expected result data to be mapped as Rates.")
                }
            }
        }
    }
    
    func testSuccessRatesBaseCurrency() {
        
        let baseCurrency = Currency.PLN
        
        stubbingProvider.request(.exchangeRates(currency: baseCurrency)) { result in
            switch result {
            case .failure(_):
                XCTAssert(false, "Expected successful result.")
            case .success(let result):
                guard let rates = try? result.map(Rates.self) else { return XCTFail("Expected result data to be mapped as Rates.") }
                XCTAssertEqual(rates.baseCurrency, baseCurrency, "Expected base currency to be equal.")
            }
        }
    }
    
    func testSuccessRatesCurrencies() {
        
        let baseCurrency = Currency.PLN
        
        stubbingProvider.request(.exchangeRates(currency: baseCurrency)) { result in
            switch result {
            case .failure(_):
                XCTAssert(false, "Expected successful result.")
            case .success(let result):
                guard let rates = try? result.map(Rates.self) else { return XCTFail("Expected result data to be mapped as Rates.") }
                XCTAssertEqual(rates.rates.count, 5)
                XCTAssertEqual(rates.baseCurrency, .PLN)
                XCTAssertTrue(rates.rates.keys.contains(.AUD))
                XCTAssertTrue(rates.rates.keys.contains(.PLN))
            }
        }
    }
    
    func testErrorStatusCode() {
        
        let endpointClosure = { (target: RevolutService) -> Endpoint in
            return Endpoint(url: URL(target: target).absoluteString,
                            sampleResponseClosure: { .networkError(NSError(domain: "", code: 1, userInfo: nil)) },
                            method: target.method,
                            task: target.task,
                            httpHeaderFields: target.headers)
        }
        
        let stubbingProvider = MoyaProvider<RevolutService>(endpointClosure: endpointClosure, stubClosure: MoyaProvider.immediatelyStub)
        
        stubbingProvider.request(.exchangeRates(currency: .PLN)) { result in
            switch result {
            case .failure(let error):
                XCTAssertNotNil(error)
            case .success(_):
                XCTAssert(false, "Expected failure result.")
            }
        }
        
    }
}

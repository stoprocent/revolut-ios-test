//
//  RevolutTestCurrencyExchangeListViewModel.swift
//  Revolut Test Tests
//
//  Created by Marek Serafin on 09/02/2019.
//  Copyright © 2019 Marek Serafin. All rights reserved.
//

import XCTest
import Moya
@testable import Revolut_Test


class RevolutTestCurrencyExchangeListViewModel: XCTestCase {
    
    let stubbingProvider = MoyaProvider<RevolutService>(stubClosure: MoyaProvider.immediatelyStub)
    
    var delegateMock: CurrenciesExchangeListViewModelDelegateMock!
    var viewModel: CurrenciesExchangeListViewModel!
    
    override func setUp() {
        delegateMock = CurrenciesExchangeListViewModelDelegateMock()
        
        viewModel = CurrenciesExchangeListViewModel(currency: .EUR, provider: stubbingProvider, interval: 1)
        viewModel.delegate = delegateMock
    }
    
    override func tearDown() {
        viewModel = nil
        delegateMock = nil
    }
    
    func testNotReady() {
        XCTAssertFalse(viewModel.isReady)
    }
    
    func testIsReady() {
        viewModel.scheduleRatesUpdate()
        XCTAssertTrue(viewModel.isReady)
    }
    
    func testAmountDidChange() {
        let expect = expectation(description: "Delegate method to be called after baseAmount was changed.")
        
        delegateMock.amountDidChange = { amount in
            expect.fulfill()
            XCTAssertEqual(100, amount)
        }
        
        viewModel.baseAmount = 100
        waitForExpectations(timeout: 1.0)
    }
    
    func testCurrencyDidChange() {
        let currency: Currency = .PLN
        let expect = expectation(description: "Delegate method to be called after baseCurrency was changed.")
        
        delegateMock.currenciesDidChange = {
            expect.fulfill()
            XCTAssertEqual(self.viewModel.baseCurrency, currency)
        }
        
        viewModel.baseCurrency = currency
        waitForExpectations(timeout: 1.0)
    }
    
    func testCurrenciesOrder() {
        let currency: Currency = .RUB
        let expect = expectation(description: "Delegate method to be called after updating base currency.")
        
        delegateMock.currenciesDidChange = {
            expect.fulfill()
            XCTAssertEqual(self.viewModel.currencies.first, currency)
        }
        
        viewModel.baseCurrency = currency
        waitForExpectations(timeout: 1.0)
    }
    
    func testCurrenciesOrderMultiple() {
    
        let expect = expectation(description: "Delegate method to be called same amount of times as base currency has changed.")
        expect.expectedFulfillmentCount = 3
        
        delegateMock.currenciesDidChange = {
            expect.fulfill()
        }
        
        viewModel.baseCurrency = .RUB
        viewModel.baseCurrency = .PLN
        viewModel.baseCurrency = .USD
        
        waitForExpectations(timeout: 1.0) { _ in
            XCTAssertEqual(self.viewModel.currencies.first, .USD)
            
        }
    }
    
    func testScheduledRatesUpdate() {
        viewModel.ratesUpdateInterval = 1
        viewModel.baseCurrency = .USD
        
        let expect = expectation(description: "More than one currency scheduled update.")
        expect.expectedFulfillmentCount = 2
        expect.assertForOverFulfill = false
        
        delegateMock.rateDidChange = { curency, _ in
            guard curency == .USD else { return }
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 3.0)
    }

    func testScheduledRatesUpdateValues() {
        
        let expect = expectation(description: "More than one currency scheduled update.")
        expect.expectedFulfillmentCount = 5
        
        delegateMock.rateDidChange = { curency, rate in
            switch curency {
            case .USD: XCTAssertEqual(rate, 1.0)
            case .AUD: XCTAssertEqual(rate, 2.0)
            case .BGN: XCTAssertEqual(rate, 3.0)
            case .BRL: XCTAssertEqual(rate, 4.0)
            case .CAD: XCTAssertEqual(rate, 5.0)
            default: XCTFail()
            }
            expect.fulfill()
        }
        
        viewModel.baseCurrency = .USD
        waitForExpectations(timeout: 2.0)
    }
    
}


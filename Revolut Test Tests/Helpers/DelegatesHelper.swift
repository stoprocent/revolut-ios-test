//
//  Mock.swift
//  Revolut Test Tests
//
//  Created by Marek Serafin on 11/02/2019.
//  Copyright © 2019 Marek Serafin. All rights reserved.
//

import XCTest
@testable import Revolut_Test

class CurrenciesExchangeListViewModelDelegateMock: CurrenciesExchangeListViewModelDelegate {
    
    var amountDidChange: ((Double) -> Void)?
    var rateDidChange: ((Currency, Double) -> Void)?
    var currenciesDidChange: (() -> Void)?
    var currenciesDidFail: ((Error) -> Void)?
    var currenciesDidReorder: ((Int, Int) -> Void)?
    
    func amountDidChange(_ viewModel: CurrenciesExchangeListViewModel, amount: Double) {
        amountDidChange?(amount)
    }
    
    func rateDidChange(_ viewModel: CurrenciesExchangeListViewModel, currency: Currency, rate: Double) {
        rateDidChange?(currency, rate)
    }
    
    func currenciesDidChange(_ viewModel: CurrenciesExchangeListViewModel) {
        currenciesDidChange?()
    }
    
    func currenciesDidFail(_ viewModel: CurrenciesExchangeListViewModel, with error: Error) {
        currenciesDidFail?(error)
    }
    
    func currenciesDidReorder(_ viewModel: CurrenciesExchangeListViewModel, index: Int, to: Int) {
        currenciesDidReorder?(index, to)
    }
}

class CurrencyExchangeCellViewModelDelegateMock: CurrencyExchangeCellViewModelDelegate {
    
    var didUpdateRateValue: ((String?) -> Void)?
    
    func didUpdateRateValue(_ viewModel: CurrencyExchangeCellViewModel, value: String?) {
        didUpdateRateValue?(value)
    }
}

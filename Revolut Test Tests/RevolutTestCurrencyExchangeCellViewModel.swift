//
//  RevolutTestCurrencyExchangeListViewModel.swift
//  Revolut Test Tests
//
//  Created by Marek Serafin on 09/02/2019.
//  Copyright © 2019 Marek Serafin. All rights reserved.
//

import XCTest
import Moya
@testable import Revolut_Test

class RevolutTestCurrencyExchangeCellViewModel: XCTestCase {
    
    let stubbingProvider = MoyaProvider<RevolutService>(stubClosure: MoyaProvider.immediatelyStub)
    
    var delegateMock: CurrencyExchangeCellViewModelDelegateMock!
    var viewModel: CurrencyExchangeCellViewModel!
    
    override func setUp() {
        delegateMock = CurrencyExchangeCellViewModelDelegateMock()
        
        viewModel = CurrencyExchangeCellViewModel(currency: .EUR, rate: 10, amount: 1)
        viewModel.delegate = delegateMock
    }
    
    override func tearDown() {
        viewModel = nil
        delegateMock = nil
    }
    
    func testSimpleModel() {
        viewModel = CurrencyExchangeCellViewModel(currency: .PLN, rate: 200, amount: 100)
        XCTAssertEqual(viewModel.currency, .PLN)
        XCTAssertEqual(viewModel.rate, 200)
        XCTAssertEqual(viewModel.amount, 100)
    }
    
    func testRateAndAmountUpdate() {
        let expectRate = expectation(description: "Rate value is updated and total value is modified.")
        let expectAmount = expectation(description: "Amount value is updated and total value is modified.")
        
        XCTAssertEqual(self.viewModel.amount, 1)
        XCTAssertEqual(self.viewModel.rate, 10.0)
        XCTAssertEqual(self.viewModel.value, "10\(Locale.current.decimalSeparator!)00")
        
        delegateMock.didUpdateRateValue = { value in
            XCTAssertEqual(self.viewModel.amount, 1)
            XCTAssertEqual(self.viewModel.rate, 20.0)
            XCTAssertEqual(value, "20\(Locale.current.decimalSeparator!)00")
            expectRate.fulfill()
        }
        viewModel.rate = 20.0
        
        
        delegateMock.didUpdateRateValue = { value in
            XCTAssertEqual(self.viewModel.amount, 10)
            XCTAssertEqual(self.viewModel.rate, 20.0)
            XCTAssertEqual(value, "200\(Locale.current.decimalSeparator!)00")
            expectAmount.fulfill()
        }
        viewModel.amount = 10
        
        XCTWaiter().wait(for: [expectRate, expectAmount], timeout: 2)
    }
}

